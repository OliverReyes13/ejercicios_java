import java.util.List;
public class FP_Functional_Exercises {
    public static void main(String[] args){
        List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
        
        List<String> courses = List.of ("Spring", "Spring Boot", "API",
                    "Microservices", "AWS", "PCF", "Azure", "Docker", "Kubernetes");
    
        /*---------------------Ejercicio 1 ----------------------*/
        System.out.println("\n Ejercicio #1: ");
        printEvenNumbersInListFunctional(numbers);
        
        /*---------------------Ejercicio 2 ----------------------*/
        System.out.println("\n Ejercicio #2: ");
        printAllCourses(courses);
        /*---------------------Ejercicio 3-----------------------*/
        System.out.println("\n Ejercicio #3: ");
        printOnlineCourses(courses);   
        /*---------------------Ejercicio 4-----------------------*/
        System.out.println("\n Ejercicio #4: ");
        printOnFourCourses(courses);       
        /*---------------------Ejercicio 5-----------------------*/
        System.out.println("\n Ejercicio #5: ");
        printSquaresOfEvenNumbersInListFunctional(numbers);               
        
        /*---------------------Ejercicio 6-----------------------*/
        System.out.println("\n Ejercicio #6: ");
        printNumberCharactersOfCoursesInListFunctional(courses);
    }
        /*metodo para imprimir numeros*/
   private static  void print(int number){
       System.out.println(number + " ");
   
    }
    /*metodo para imprimir la lista courses,Metodo del ejercicio 2*/
    private static void printS(String courses){
        System.out.println(courses+ " ");
    }
     
   /*metodo para ejercicio 1*/
   private static boolean isEven(int number){
       return(number % 2 != 0 );
   }
  /*metodo para ejercicio 5*/
  private static boolean isPar(int number){
    return(number % 2 == 0 );
}

   /*---------------------Ejercicio 1 ----------------------*/
   private static void printEvenNumbersInListFunctional(List<Integer> numbers){
       numbers.stream()
               .filter(FP_Functional_Exercises::isEven)
               .forEach(FP_Functional_Exercises::print);
       System.out.println("");
   }
   
   /*---------------------Ejercicio 2 ----------------------*/
   private static void printAllCourses(List<String> courses){
       courses.stream()
               .forEach(FP_Functional_Exercises::printS);
       System.out.println("");
   }
   /*-------------------Ejercicio 3------------------------*/

   private static void printOnlineCourses(List<String>courses){
        courses.stream()
        .filter(course -> course.contains ("Spring"))
        .forEach (FP_Functional_Exercises::printS);
        System.out.println("");
   }

      /*-------------------Ejercicio 4------------------------*/

      private static void printOnFourCourses(List<String>courses){
        courses.stream()
        .filter(course -> course.length()>= 4)
        .forEach (FP_Functional_Exercises::printS);
        System.out.println("");
   }
   
      /*-------------------Ejercicio 5------------------------*/
   private static void printSquaresOfEvenNumbersInListFunctional(List<Integer> numbers){
    numbers.stream()                        
        .filter(FP_Functional_Exercises::isPar)
        .map(number -> number * number)   
        .forEach(FP_Functional_Exercises::print);
    System.out.println("");
}
      /*-------------------Ejercicio 6------------------------*/
      private static void printNumberCharactersOfCoursesInListFunctional(List<String> courses){
        courses.stream()
            .map(course -> course.length())                         
            .forEach(FP_Functional_Exercises::print);
        System.out.println("");
    }

}
